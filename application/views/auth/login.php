
<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-6 col-lg-6 col-md-6">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><?= $heading ;?></h1>
                  </div>
                  <div class="information alert"></div>
                  <form class="user" data-uri='<?= base_url();?>/auth/check_validation'>
                    <div class="form-group">
                      <input type="text" name="username" class="form-control" placeholder="Enter Your Username ...">
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control" placeholder="Enter Your Password ...">
                    </div>
                    
                  </form>
                  <button id="log" class="btn btn-primary btn-user btn-lock"> <i class="fa fa-lock"></i> Login</button>

                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>