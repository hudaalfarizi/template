<?php

class Response extends CI_Model
{

	function sendresponse($res, $data = 'data tidak ditemukan')
	{
		$CI = &get_instance();
		return $CI->output->set_content_type('application/json')
						  ->set_status_header($res)
						  ->set_output(json_encode($data));
	}

}