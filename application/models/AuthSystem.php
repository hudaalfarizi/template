<?php
/**
 * 
 */
class AuthSystem extends CI_Model
{
	
	public function check()
	{
		if(isset($_POST['username']))
		{
			$validuname = $this->db->get_where('users',['username'=>$_POST['username']])->row_array();
			if($validuname)
			{
				if($validuname['password'] == $_POST['password'])
				{
					$data = [
						'res' => 'success',
						'msg' => 'anda berhasil login',
					];

					$this->response->sendresponse(200,$data);
				} else {
					$data = [
						'res' => 'fail',
						'msg' => 'password anda salah',
					];
					$this->response->sendresponse(200,$data);
				}
			} else {
				$data = [
						'res' => 'fail',
						'msg' => 'username anda tidak terdaftar',
					];
				$this->response->sendresponse(200,$data);
			}
		}
	}
	
}