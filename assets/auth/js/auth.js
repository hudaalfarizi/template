$('#log').on('click',function(){

    $.ajax({
        url      : $('.user').data('uri'),
        data     : $('.user').serialize(),
        dataType : 'JSON',
        type     : 'POST',
        beforeSend: function () {
            $('.information').show();
            $('.information').addClass('alert-info');
            $('.information').html('loading...');
		},
        success  : function(response)
        {
            if(response.res == 'fail')
            {
                $('.information').show();
                $('.information').addClass('alert-danger');
                $('.information').html(response.msg);
            } else {
                //redirect
                $('.information').show();
                $('.information').addClass('alert-success');
                $('.information').html(response.msg);
            }
        }
    })
})